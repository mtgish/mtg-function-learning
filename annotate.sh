mkdir output
while read p; do
  echo "Beginning $p"
  java -cp bin/stanford-classifier.jar edu.stanford.nlp.classify.ColumnDataClassifier -prop base.prop -loadClassifier models/$p-mod.ner.gz -testFile annotate/$p.annt > output/$p.final
done <functions.txt

#java -cp stanford-classifier.jar edu.stanford.nlp.classify.ColumnDataClassifier -prop training/base.prop -loadClassifier models/a1-mod.ner.gz -testFile annotate/a1.annt > output/a1.final
