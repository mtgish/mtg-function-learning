# mkdir models
while read p; do
  echo "Beginning $p"
  java -cp bin/stanford-classifier.jar edu.stanford.nlp.classify.ColumnDataClassifier -prop base.prop -trainFile training/$p.train -testFile training/$p.test -serializeTo models/$p-mod.ner.gz > output/$p.out
done <functions.txt

#java -cp stanford-classifier.jar edu.stanford.nlp.classify.ColumnDataClassifier -prop training/base.prop -trainFile training/a1.train -testFile training/a1.test -serializeTo models/a1-mod.ner.gz > output/a1.out
