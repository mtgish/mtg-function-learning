mkdir output
while read p; do
  echo "Beginning $p"
  java -cp bin/stanford-classifier.jar edu.stanford.nlp.classify.ColumnDataClassifier -prop base.prop -loadClassifier models/$p-mod.ner.gz -testFile testdata/$p-x.test > output/$p-x.out
done <functions.txt

#java -cp stanford-classifier.jar edu.stanford.nlp.classify.ColumnDataClassifier -prop training/base.prop -loadClassifier models/a1-mod.ner.gz -testFile testdata/a1-x.test > output/a1-x.out
