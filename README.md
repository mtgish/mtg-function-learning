This project is to train/test machine learning for functions

training contains the files to be used to train the model

testdata is used for data to be tested

models contains the actual constructed models

annotate contains the files to be annotated

testmodels.sh is used to create and test models in one step

testxfiles.sh is used to only test the model

annotate.sh is used to generate annotated files
